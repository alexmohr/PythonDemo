import time
import pygame
import random
import math


class FoodHandler:
    def __init__(self, screen):
        self.screen = screen
        self.blocksize = 10
        self.color = (255, 0, 0)  # food is red
        self.newFood()

    def draw(self):
        pygame.draw.rect(
            self.screen,
            self.color,
            (self.x, self.y, self.blocksize, self.blocksize))

    def getLocation(self):
        return [self.x, self.y]

    def newFood(self):
        # use the 10px as border
        self.x = roundup(
            random.randint(
                self.blocksize,
                self.screen.get_width() - self.blocksize),
            self.blocksize)

        self.y = roundup(
            random.randint(
                self.blocksize,
                self.screen.get_height() - self.blocksize),
            self.blocksize)

        print("New food at " + str(self.x) + ":" + str(self.y))
        self.draw()


class SnakeHandler:
    def __init__(self, screen):
        self.blocksize = 10
        self.stepsize = 1
        self.eatSize = 3

        self.color = (0, 255, 0)
        self.headColor = (255, 255, 255)
        self.screen = screen
        self.position = []
        startX = 100
        startY = 100
        for i in range(0, 3):  # 3 is minim block size
            self.position.append([
                startX - i * self.blocksize,
                startY])

    def draw(self, move):
        if move[0] != 0 or move[1] != 0:
            for j in range(len(self.position) - 1, 0, -1):
                self.position[j][0] = self.position[j - 1][0]
                self.position[j][1] = self.position[j - 1][1]

        self.position[0][0] += move[0]
        self.position[0][1] += move[1]

        # wrap field arround
        width = screen.get_width()
        height = screen.get_height()
        x = self.position[0][0]
        y = self.position[0][1]
        if x >= width:
            x = 0
        elif x < 0:
            x = width
        elif y >= height:
            y = 0
        elif y < 0:
            y = height
        self.position[0][0] = x
        self.position[0][1] = y

        for i in range(0, len(self.position)):
            pos = self.position[i]
            color = self.color
            if 0 == i:
                color = self.headColor
            pygame.draw.rect(
                self.screen,
                color,
                (pos[0], pos[1], self.blocksize, self.blocksize))

    def getLocation(self):
        return self.position[0]

    def eat(self, pos):
        for i in range(0, self.eatSize):
            lastPos = self.position[len(self.position) - 1]
            newPos = [lastPos[0] + pos[0], lastPos[1] + pos[1]]
            self.position.append(newPos)
        snake.draw(pos)

    def isColliding(self, obj):
        for oPos in obj:
            for sPos in self.position:
                if oPos[0] == sPos[0] and oPos[1] == sPos[1]:
                    return True
        return False

    def crashedToSelf(self):
        x = self.position[0][0]
        y = self.position[0][1]
        for i in range(1, len(self.position) - 1, 1):
            px = self.position[i][0]
            py = self.position[i][1]
            if (px == x and py == y):
                # Player is dead
                return True

        return False


def milliTime():
    millis = int(round(time.time() * 1000))
    return millis


def roundup(x, ceiling):
    return int(math.ceil(x / ceiling)) * ceiling


if __name__ == "__main__":

    # create a game window
    pygame.init()
    pygame.display.set_caption("Snake(s) on a game")
    screen = pygame.display.set_mode((640, 400))
    background = [0, 0, 0]  # black

    running = True
    lost = False
    snake = SnakeHandler(screen)
    food = FoodHandler(screen)
    lastUpdate = 0
    updateIntervall = 50
    points = 0

    moveSize = 10
    move = [0, 0]

    # main loop
    while running:
        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT and move[0] >= 0:
                move = [moveSize, 0]
            elif event.key == pygame.K_LEFT and move[0] <= 0:
                move = [moveSize * -1, 0]
            elif event.key == pygame.K_DOWN and move[1] >= 0:
                move = [0, moveSize]
            elif event.key == pygame.K_UP and move[1] <= 0:
                move = [0, moveSize * -1]

        timeNow = milliTime()
        if (timeNow - lastUpdate > updateIntervall):
            # This will update the screen
            screen.fill(background)
            food.draw()
            snake.draw(move)
            if (snake.crashedToSelf()):
                running = False
                lost = True

            foodLocation = food.getLocation()
            snakeLocation = snake.getLocation()
            if (foodLocation[0] == snakeLocation[0] and
                    foodLocation[1] == snakeLocation[1]):
                snake.eat(move)
                points += 1

                food.newFood()
                while snake.isColliding([food.getLocation()]):
                    food.newFood()

            lastUpdate = timeNow
        pygame.display.flip()

    # Display loose text
    if lost:
        # initialize font; must be called after 'pygame.init()'
        myfont = pygame.font.SysFont("monospace", 36)

        # render text
        label_over = myfont.render("GAME OVER!", 1, (255, 0, 0))
        label_points = myfont.render(
            "You have reached " + str(points) + " point(s)", 1, (255, 0, 0))

        game_rect = label_over.get_rect(
            center=(screen.get_width() / 2, screen.get_height() / 3))
        point_rect = label_points.get_rect(
            center=(screen.get_width() / 2,
                    screen.get_height() / 2))

        screen.blit(label_over, game_rect)
        screen.blit(label_points, point_rect)

        pygame.display.flip()

        running = True
        while running:
            event = pygame.event.poll()
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                running = False
