##
# Basic Python script
##

# import turtle module
import turtle

# contains methods for sleep-
import time

# seutp window
wn = turtle.Screen()
wn.bgcolor("lightgreen")        # set the window background color

# setup tess the turtle
tess = turtle.Turtle()
tess.color("blue")              # make tess blue
tess.pensize(3)                 # set the width of her pen
tess.shape("turtle")


# move tess 50 points forward
tess.forward(50)
time.sleep(1)

# turn tess 120° to the left
tess.left(120)
time.sleep(1)

# move again 50 points
tess.forward(50)

wn.exitonclick()               # wait for a user click on the canvas
