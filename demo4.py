##
# Datatypes
##

# Lets forget about tess for a bit and do a litle bit of math.

import math


def calc_circle_area(radius, pi):
    # area of a circle is PI* radius^2
    rSquared = math.pow(radius, 2)
    area = pi * rSquared

    # functions can do work for us and give us the result back.
    return area


# Define a "main" function
# This will be executed when the programm is loaded
if __name__ == "__main__":

    # A string represents a collection of characters
    # or a single character
    print("Strings")
    string = "Cat"
    char = "c"

    print("String:" + string)
    print("Char:" + string)

    # Integer allow us to do math and stuff.
    radius = 10
    anotherInt = 24
    addition = radius + 42

    # waiiiit why can we print "math" without converting it to a string??
    # The print function is pretty smart.
    # Everything which is not already a string will be converted into one.
    # We needed the conversion in the last chapter because we joined
    # multiple datatypes.
    # \n prints an empty line
    print("\nAddition: " + str(addition))

    # This would crash
    # print("math: " + math)

    # this won't
    print("addition: " + str(addition))

    # python supports floating point numbers
    pi_small = 3.1415

    # we can give the result of a function directly into another one.
    # You can also provide parameters in the defined order instead with a name
    # \t prints a "tab" char this formats the output
    print("\nCalcaluating areas:")
    small_pi = calc_circle_area(radius, pi_small)
    actual_pi = calc_circle_area(radius, math.pi)
    print("Small pi\t" + str(small_pi))
    print("Actual pi\t" + str(actual_pi))

    # Conditional statements
    if (small_pi > actual_pi):
        print("Small pi is larger than actual pi")
    else:
        print("Actual pi is bigger than small pi")

    ##
    # loops
    ##

    # do something until a condition is reached
    print("\nWhile radius > 0")
    while radius > 0:
        print("radius: " + str(radius))
        radius -= 1

    # do something 3 times
    print("\nDo somehting 3 times")
    for x in range(0, 3):
        # increment radius by 1
        radius += 1
        print("new radius: " + str(radius))

    # Collections

    # we will collect a number of areas
    area_collection = []
    areas_to_collect = 5
    print("\nCollect " + str(areas_to_collect) + " areas")
    for x in range(0, areas_to_collect):
        # increment radius by 1
        radius += 1
        newArea = calc_circle_area(radius, math.pi)
        area_collection.append(newArea)

    # print the areas we have collected
    print("\ndirect array access")
    for area in area_collection:
        print(area)

    # print the areas we have collected
    print("\nindexed array access")
    for i in range(0, len(area_collection)):
        print("Area " + str(i) + ": " + str(area_collection[i]))

    # Key value storage
    key_value_store = {}
    key_value_store["Cat"] = "Blue"
    key_value_store["Dog"] = "Red"
    key_value_store["Ship"] = "Horse"

    print("\nKeyvalue store")
    for entry in key_value_store:
        print(entry + ":" + key_value_store[entry])
