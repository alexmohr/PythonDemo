##
# Global variables
##


# import turtle module
import turtle

# contains methods for sleep-
import time

###
# Define GLOBAL variables
###

# This variable can be accessed by anyone.
# we mark global variables with "_" so we now they are global
# often m_ is used as an alternative
# m_ stands for "member" because we variable is a member of this file
# and not a so called local variable,
# which is a variable declared inside a method.
_forwardMove = 50


#
# @brief      Defines a function which rotates tess to the left
#
# @param      tess    Our lovely turtle tess
# @param      degree  Degree tess will rotate
#
def tess_move_left(tess, degree):
    tess.left(degree)
    time.sleep(1)


#
# @brief      Defines a function which moves tess forward.
#
# @param      tess    Our lovely turtle tess
# @param      points  How many point will tess move forwad
#
def tess_move_forward(tess, points):
    tess.forward(points)
    time.sleep(1)


def show_tess_speed():
    # "Print" the value of the variable to our conole
    print(
        "Tess is moving forward with " +
        # because our variable is an integer we have to convert it to a string
        # More about datatypes in the next demo.
        str(_forwardMove) +
        " points")


# Define a "main" function
# This will be executed when the programm is loaded
if __name__ == "__main__":
    # seutp window
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")        # set the window background color

    # setup tess the turtle
    # Tess now has a bit different name
    tessTheTurtle = turtle.Turtle()
    tessTheTurtle.color("blue")              # make tess blue
    tessTheTurtle.pensize(3)                 # set the width of her pen
    tessTheTurtle.shape("turtle")

    # How fast is tess?
    show_tess_speed()

    # Now we tell tess to move her lazy turtle ass.
    # as you can see the parameter named "tess" receives the value
    # stored in our variable tessTheTurtle
    # the parameter points gets a value defined by us.
    tess_move_forward(tess=tessTheTurtle, points=_forwardMove)

    # turn tess 120° to the left
    tess_move_left(tess=tessTheTurtle, degree=120)

    # move again 50 points
    # move tess forward again.
    tess_move_forward(tess=tessTheTurtle, points=_forwardMove)

    wn.exitonclick()               # wait for a user click on the canvas
