##
# Methods
##

# import turtle module
import turtle

# contains methods for sleep-
import time


#
# @brief      Defines a function which rotates tess to the left
#
# @param      tess    Our lovely turtle tess
# @param      degree  Degree tess will rotate
#
def tess_move_left(tess, degree):
    tess.left(degree)
    time.sleep(1)


#
# @brief      Defines a function which moves tess forward.
#
# @param      tess    Our lovely turtle tess
# @param      points  How many point will tess move forwad
#
def tess_move_forward(tess, points):
    tess.forward(points)
    time.sleep(1)


# Define a "main" function
# This will be executed when the programm is loaded
if __name__ == "__main__":
    # seutp window
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")        # set the window background color

    # setup tess the turtle
    # Tess now has a bit different name
    tessTheTurtle = turtle.Turtle()
    tessTheTurtle.color("blue")              # make tess blue
    tessTheTurtle.pensize(3)                 # set the width of her pen
    tessTheTurtle.shape("turtle")

    # Now we tell tess to move her lazy turtle ass.
    # as you can see the parameter named "tess" receives the value
    # stored in our variable tessTheTurtle
    # the parameter points gets a value defined by us.
    tess_move_forward(tess=tessTheTurtle, points=50)

    # turn tess 120° to the left
    tess_move_left(tess=tessTheTurtle, degree=120)

    # move again 50 points
    # move tess forward again.
    tess_move_forward(tess=tessTheTurtle, points=50)

    # So functions allow us to group bits of code together.
    # They are used to give blocks of code a propper
    # name and they can be reused as often as you like.
    wn.exitonclick()               # wait for a user click on the canvas
