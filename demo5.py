##
# Fibonacci
##


# import turtle module
import turtle

###
# Define GLOBAL variables
###

# This variable can be accessed by anyone.
# we mark global variables with "_" so we now they are global
# often m_ is used as an alternative
# m_ stands for "member" because we variable is a member of this file
# and not a so called local variable,
# which is a variable declared inside a method.
_forwardMove = 50
_maxFiboNumber = 1000


#
# @brief      Defines a function which rotates tess to the left
#
# @param      tess    Our lovely turtle tess
# @param      degree  Degree tess will rotate
#
def tess_move_left(tess, degree):
    tess.left(degree)


#
# @brief      Defines a function which moves tess forward.
#
# @param      tess    Our lovely turtle tess
# @param      points  How many point will tess move forwad
#
def tess_move_forward(tess, points):
    tess.forward(points)


# Define a "main" function
# This will be executed when the programm is loaded
if __name__ == "__main__":
    # seutp window
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")        # set the window background color

    # setup tess the turtle
    # Tess now has a bit different name
    tessTheTurtle = turtle.Turtle()
    tessTheTurtle.color("blue")              # make tess blue
    tessTheTurtle.pensize(1)                 # set the width of her pen
    tessTheTurtle.shape("turtle")

    fibonacci_last = 1
    fibonacci_now = 1

    while fibonacci_now < _maxFiboNumber:
        tess_move_forward(tess=tessTheTurtle, points=fibonacci_now)
        tess_move_left(tess=tessTheTurtle, degree=90)

        tmp = fibonacci_now
        fibonacci_now = fibonacci_now + fibonacci_last
        fibonacci_last = tmp

    # setup tess the turtle
    # Tess now has a bit different name
    tina = turtle.Turtle()
    tina.color("red")              # make tess blue
    tina.pensize(1)                 # set the width of her pen
    tina.shape("turtle")
    tina.right(90)
    for x in range(1, 100, 1):
        tina.stamp()                # leave an impression on the canvas
        tina.forward(x)          # move tess along
        tina.left(20)

    wn.exitonclick()               # wait for a user click on the canvas
